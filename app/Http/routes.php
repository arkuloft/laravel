<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(DB::connection()->getDatabaseName())
    {
        $data['message']='Соединение установлено, продолжаем';
    }
    else {
        $data['message']='Ошибка соединения с БД';
    }
    return view('welcome',$data);
});
Route::get('/test', function(){
    return \App\Items::with('category')->first();
});

Route::get('/categories/', 'CategoriesController@index');
Route::get('/categories/create', 'CategoriesController@create');
Route::get('/categories/{id}', 'CategoriesController@show');
Route::get('/categories/{id}/edit', 'CategoriesController@edit');
Route::patch('/categories/{id}', 'CategoriesController@update');
Route::post('/categories', 'CategoriesController@store');
Route::get('/categories/{id}/destroy/', 'CategoriesController@destroy');

//Route::resource('categories','CategoriesController');
Route::resource('items','ItemsController');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
