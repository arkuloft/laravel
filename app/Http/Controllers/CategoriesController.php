<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $data['categories']=$categories;
        return view('categories.index',$data);
    }

    public function show($id)
    {
        $data['category'] = Category::find($id);
        return view('categories.show',$data);
    }


    public function edit($id)
    {
        $data['category'] = Category::find($id);
        return view('categories.edit',$data);
    }
    public function update(Request $request,$id){
        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->save();

        return redirect('/categories/'.$id);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->save();

        return redirect('/categories/'.$category->id);
    }

    public function destroy($id)
    {
        Category::destroy($id);
        return redirect('/categories/');
    }
}
