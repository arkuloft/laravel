@extends('layout.mainlayout')

@section('content')
    <h1>Создать новую категорию</h1>
    <div class="row">
        <div class="col-md-4">
            <form action="/categories/" method="post">
                {{csrf_field()}}
                Name: <input type="text" name="name" class="form-control"> <br>
                slug: <input type="text" name="slug" class="form-control"> <br>
                <input type="submit">
            </form>
         </div>
        <div class="col-md-4"></div>
    </div>
    <a href="{{URL::previous()}}">Назад</a>
@endsection