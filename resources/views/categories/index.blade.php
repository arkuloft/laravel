@extends('layout.mainlayout')

@section('content')
    <h1>Категории - производители</h1>
    <h4><a href="/categories/create">
        Создать
        </a>
    </h4>
    <table class="table">
        @foreach($categories as $category)
            <tr>
                <td><a href="/categories/{{$category->id}}">{{$category->id}}</a></td>
                <td><a href="/categories/{{$category->id}}">{{$category->name}}</a></td>
                <td><a href="/categories/{{$category->id}}">{{$category->slug}}</a></td>
                <td>
                    <a href="/categories/{{$category->id}}/edit">Редактировать</a>
                    ||
                    <a href="/categories/{{$category->id}}/destroy/">Удалить</a>

                </td>
            </tr>
        @endforeach
    </table>
@endsection