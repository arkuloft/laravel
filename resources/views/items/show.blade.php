@extends('layout.mainlayout')

@section('content')
    <h1>Товар #{{$item->id}} из категории {{$item->category->name or ''}}</h1>
    <ul>
        <li>Название:{{$item->name}}</li>
        <li>slug: {{$item->slug}}</li>
        <samp>
            {{$item->description}}
        </samp>
    </ul>
    <a href="{{URL::previous()}}">Назад</a>
@endsection