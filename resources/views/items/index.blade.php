@extends('layout.mainlayout')

@section('content')
    <h1>Товары</h1>
    <h4><a href="/items/create">
            Создать
        </a>
    </h4>
    <table class="table">
        @foreach($items as $item)
            <tr>
                <td><a href="/items/{{$item->id}}">{{$item->id}}</a></td>
                <td><a href="/items/{{$item->id}}">{{$item->name}}</a></td>
                <td><a href="/items/{{$item->id}}">{{$item->slug}}</a></td>
                <td>
                    <a href="/items/{{$item->id}}/edit">Редактировать</a>
                    ||
                    <a href="/items/{{$item->id}}/destroy/">Удалить</a>

                </td>
            </tr>
        @endforeach
    </table>
@endsection