<html>
<head>
<script src="/js/bootstrap.min.js"></script>
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="/">Главная</a></li>
    <li role="presentation"><a href="/categories/">Категории</a></li>
    <li role="presentation"><a href="/items/">Товары</a></li>
</ul>

@yield('content')
</body>
</html>