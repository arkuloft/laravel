@extends('layout.mainlayout')

@section('content')
    <h1>Главная страница</h1>
    <h4>Статус: {{$message or ''}}</h4>
    <b>Для наполнения тестовыми данными - выполните в консоли: </b>
    <ul>
        <li>php artisan migrate</li>
        <li>php artisan db:seed --class TablesSeeder</li>
    </ul>
@endsection