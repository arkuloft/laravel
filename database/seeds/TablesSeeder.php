<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('ru_RU');
        foreach (range(1,15) as $index) {
            DB::table('categories')->insert([
                'name' => $faker->company(),
                'slug' => $faker->company(),
            ]);
        }

        $faker = Faker::create();
        foreach (range(1,50) as $index) {
            DB::table('items')->insert([
                'name' => $faker->numerify('Стелс ###'),
                'slug' => $faker->numerify('Stels-###'),
                'description' => $faker->paragraph(),
                'category_id' => $faker->numberBetween($min = 1, $max = 15),
            ]);
        }
    }
}
